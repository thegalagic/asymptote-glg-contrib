#!/bin/env bats

# SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Asyunit is a test runner for the Asymptote vector graphics language.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/asyunit/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

load 'test_helper/bats-support/load'
load 'test_helper/bats-assert/load'
load 'test_helper/test_code'

@test "all args missing, print usage" {
  run_usage

  assert_failure
  assert_output --partial "Usage: "
}

@test "fixture missing, fail" {
  run_usage badfixture

  assert_failure
  assert_output "fixture not found: badfixture"
}

@test "print version" {
  run_usage -v

  assert_success
  assert_output --regexp "asyunit [0-9]+.[0-9]+"
}

@test "print help" {
  run_usage -h

  assert_success
  assert_output --partial "Usage: "
}

# TODO: Test that filters work against test functions
