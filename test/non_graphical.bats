#!/bin/env bats

# SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Asyunit is a test runner for the Asymptote vector graphics language.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/asyunit/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

load 'test_helper/bats-support/load'
load 'test_helper/bats-assert/load'
load 'test_helper/test_code'

# These are used as global variables
test_code=${test_code:-}
test_code_file=${test_code_file:-}

@test "no test functions: no problem" {
  test_code="assert(true);"
  run_test_code
  assert_success
  assert_output ""
}

@test "no test functions: code asserts failure no problem" {
  test_code="assert(false);"
  run_test_code
  assert_success
}

@test "one test function: code is success" {
  test_code="void test_success() {
      assert(true);
    }"

  run_test_code
  assert_success
  assert_output "ok ${test_code_file} test_success"
}

@test "one test function leading whitespace: code is success" {
  test_code="
    void test_success() {
      assert(true);
    }"

  run_test_code
  assert_success
  assert_output "ok ${test_code_file} test_success"
}

@test "one test function: code is failure" {
  test_code="void test_failure() {
      assert(false);
    }"

  run_test_code
  assert_failure
  assert_output "$(cat << EOF
# assert(false);
# ^
# ${test_code_file}: 2.13: assert FAILED
not ok ${test_code_file} test_failure
EOF
  )"
}

@test "one test function: code has syntax error" {
  test_code="void test_failure() {
      assert false;
    }"

  run_test_code
  assert_failure
  assert_output "$(cat << EOF
# assert false;
# ^
# ${test_code_file}: 2.14: syntax error
# error: could not load module '${test_code_file}'
not ok ${test_code_file} test_failure
EOF
  )"
}

@test "multiple test functions: code is success" {
  test_code="void test_success() {
      assert(true);
    }

    void test_success_again() {
      assert(true);
    }"

  run_test_code
  assert_success
  assert_output "$(cat << EOF
ok ${test_code_file} test_success
ok ${test_code_file} test_success_again
EOF
)"
}
