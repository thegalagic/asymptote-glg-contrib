#!/bin/env bats

# SPDX-FileCopyrightText: 2020-3 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Asyunit is a test runner for the Asymptote vector graphics language.
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/asyunit/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.

load 'test_helper/bats-support/load'
load 'test_helper/bats-assert/load'
load 'test_helper/test_code'

# These are used as global variables
test_code=${test_code:-}
test_code_file=${test_code_file:-}

@test "expectations met" {
  test_code="void test_success() {
      draw(circle((0,0), 50));
    }"

  run_test_code_graphical test/expectations/circle.eps
  assert_output "ok ${test_code_file} test_success"
  assert_success
}

@test "shipout fails" {
  # Force shipout error with eps bounding box issue on non-existant graphic
  test_code="void test_failure() {
      label(graphic(\"blah.eps\"));
    }"

  run_test_code_graphical
  assert_failure
  # Don't test the full output as it is highly platform specific
  assert_output --partial "not ok ${test_code_file} test_failure"
}

@test "two tests using copies of currentpicture do not interfere" {
  # This is to ensure we don't change currentpicture between runs
  test_code="$(cat << EOF
picture p = currentpicture;

void test_success() {
   draw(p, circle((0,0), 50));
}

void test_another() {
   draw(p, circle((0,0), 50));
}
EOF
  )"

  run_test_code_graphical test/expectations/circle.eps
  assert_output "$(cat << EOF
ok ${test_code_file} test_success
ok ${test_code_file} test_another
EOF
  )"
  assert_success
}

@test "missing eps expectation" {
  test_code="void test_success() {
      draw(circle((0,0), 50));
    }"

  run_test_code_graphical
  assert_failure
  assert_output "$(cat << EOF
# missing expectation: ${test_code_file}_test_success_.eps
not ok ${test_code_file} test_success
EOF
)"
}

@test "missing eps, log, tex expectations" {
  test_code="void test_success() {
      draw(circle((0,0), 50));
      label(\"hello\");
    }"

  run_test_code_graphical
  assert_failure
  assert_output "$(cat << EOF
# missing expectation: ${test_code_file}_test_success_.log
# missing expectation: ${test_code_file}_test_success_0.eps
# missing expectation: ${test_code_file}_test_success_.tex
not ok ${test_code_file} test_success
EOF
)"
}

@test "diff in eps expectation" {
  test_code="void test_success() {
      draw(circle((0,0), 50));
    }"

  run_test_code_graphical test/expectations/circle_diff.eps test/expectations/circle_diff.eps.license

  assert_output "$(cat << EOF
# diff failed on files ${test_code_file}_test_success_.eps ${test_code_file/test_code/build\/test_code}_test_success_.eps
# ImageMagick diff score: 820
not ok ${test_code_file} test_success
EOF
)"
  assert_failure
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_success_.eps.diff.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_success_.eps.actual.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_success_.eps.expected.png" ]
}

@test "two tests diff in eps expectations" {
  test_code="$(cat << EOF
void test_success() {
   draw(circle((0,0), 50));
}

void test_another() {
   draw(circle((0,0), 50));
}
EOF
  )"

  run_test_code_graphical test/expectations/circle_diff.eps
  assert_output "$(cat << EOF
# diff failed on files ${test_code_file}_test_success_.eps ${test_code_file/test_code/build\/test_code}_test_success_.eps
# ImageMagick diff score: 820
not ok ${test_code_file} test_success
# diff failed on files ${test_code_file}_test_another_.eps ${test_code_file/test_code/build\/test_code}_test_another_.eps
# ImageMagick diff score: 820
not ok ${test_code_file} test_another
EOF
  )"
  assert_failure
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_success_.eps.diff.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_success_.eps.actual.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_success_.eps.expected.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_another_.eps.diff.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_another_.eps.actual.png" ]
  assert [ -e "${test_code_file/test_code/build\/test_code}_test_another_.eps.expected.png" ]
}

@test "test has expectation files but not a graphical test" {
  test_code="void test_success() {
     int i=0; 
    }"

  run_test_code test/expectations/circle.eps
  assert_output "$(cat << EOF
# expectation file found but this is not a graphical test: ${test_code_file}_test_success_.eps
not ok ${test_code_file} test_success
EOF
)"
  assert_failure
}

# TODO: Multiple diffs fail
# TODO: Running multiple tests together
